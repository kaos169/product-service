package org.pgartsev.sample.product_service.productservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pgartsev.sample.product_service.productservice.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private MockMvc mvc;


    @Test
    public void testFindProductOk() throws Exception {
        String name = "iphone-xr";
        Product product = new Product(name, "6x2.5 ГГц, 3 ГБ, IPS, 1792x828, камера 12 Мп, 3G, 4G, NFC, GPS, 2942 мА*ч");
        mvc.perform(MockMvcRequestBuilders
                .get("/product")
                .param("name",name)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(toJson(product)));
    }


    private String toJson(Product product) throws JsonProcessingException {
        return objectMapper.writeValueAsString(product);
    }
}