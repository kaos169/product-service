insert into product values ('iphone-xr', '6x2.5 ГГц, 3 ГБ, IPS, 1792x828, камера 12 Мп, 3G, 4G, NFC, GPS, 2942 мА*ч');
insert into product values ('iphone-xs', '6x2.3 ГГц, 3 ГБ, IPS, 1920x1080, камера 12+12 Мп, 3G, 4G, NFC, GPS, 2675 мА*ч');
insert into product values ('samsung-galaxy-a50', '8x2.3 ГГц, 4 ГБ, 2 SIM, Super AMOLED, 2340x1080, камера 25+8+5 Мп, 3G, 4G, NFC, GPS, 4000 мА*ч');
insert into product values ('xiaomi-redmi-note-7', '8x2.2 ГГц, 3 ГБ, 2 SIM, IPS, 2340x1080, камера 48+5 Мп, 3G, 4G, GPS, 4000 мА*ч');