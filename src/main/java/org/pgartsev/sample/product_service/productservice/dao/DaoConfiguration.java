package org.pgartsev.sample.product_service.productservice.dao;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages="org.pgartsev.sample.product_service.productservice.dao")
@EnableTransactionManagement
@EntityScan(basePackages="org.pgartsev.sample.product_service.productservice.model")
public class DaoConfiguration {
}
