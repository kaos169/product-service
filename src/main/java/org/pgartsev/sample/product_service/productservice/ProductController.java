package org.pgartsev.sample.product_service.productservice;

import org.pgartsev.sample.product_service.productservice.dao.ProductRepository;
import org.pgartsev.sample.product_service.productservice.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {

    private final ProductRepository productRepository;


    public ProductController(@Autowired ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @RequestMapping(path = "/product", method = RequestMethod.GET)
    public Product findProduct(@RequestParam(value="name") String name) {
        return productRepository.findById(name.toLowerCase())
                .orElseThrow(() -> new IllegalArgumentException("Invalid product name:" + name.toLowerCase()));
    }

    @RequestMapping(path = "/products", method = RequestMethod.GET)
    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }
}
