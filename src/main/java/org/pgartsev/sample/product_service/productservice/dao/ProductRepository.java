package org.pgartsev.sample.product_service.productservice.dao;

import org.pgartsev.sample.product_service.productservice.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, String> {

    @Override
    List<Product> findAll();
}
